import React, {Fragment} from 'react'
import ContactBanner from "../components/organisms/contact/ContactBanner";

const Contact = () => {
    return(
        <Fragment>
            <ContactBanner/>
        </Fragment>
    )
}

export default Contact